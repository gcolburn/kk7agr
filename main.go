package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"kk7agr/js8"
	"net/http"
	"time"
)

var (
	cmd     = make(chan js8.Message, 0)
	station = js8.NewStation("")
)

func main() {
	fmt.Println("Getting Callsign from JS8CALL...")
	callsign := js8.GetCallSign()
	fmt.Printf("Callsign: %s\n", callsign)
	grid := js8.GetGrid()
	fmt.Printf("Grid: %s\n", grid)

	station.Name = callsign
	station.Grid = grid

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	tmpl := template.Must(template.ParseFiles("index.html"))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl.Execute(w, station)
	})
	http.HandleFunc("/msg", sendMessageHandler)
	http.HandleFunc("/hb", hbHandler)
	http.HandleFunc("/cq", cqHandler)
	go monitorCallActivity(station, cmd)
	fmt.Println("Server Started on http://localhost:8080")
	http.ListenAndServe(":8080", nil)
}

func sendMessageHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Println("request")
	var formMessage js8.FormMsg
	err := json.NewDecoder(req.Body).Decode(&formMessage)
	if err != nil {
		fmt.Println(err)
	}
	msg := &js8.Message{
		Type:  js8.TYPE_TX_SEND,
		Value: formMessage.Message,
	}
	fmt.Println(msg)
	cmd <- *msg
}

func hbHandler(w http.ResponseWriter, req *http.Request) {
	cmd <- station.NewHBMsg()
}

func cqHandler(w http.ResponseWriter, req *http.Request) {
	cmd <- station.NewCQMsg()
}

func monitorCallActivity(s *js8.Station, cmd <-chan js8.Message) {
	for {
		select {
		case msg := <-cmd:
			err := js8.SendMessage(msg)
			if err != nil {
				fmt.Printf("error sending message: %v", err)
			}
		case <-time.After(500 * time.Millisecond):
			m := js8.GetCallActivity()
			s.Messages = append([]js8.Message{m}, s.Messages...)
			fmt.Printf("Rx: %+v\n", m)
		}
	}
}
