package js8

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"time"
)

const TYPE_TX_SETMESSAGE = "TX.SET_TEXT"
const TYPE_TX_SEND = "TX.SEND_MESSAGE"
const RX_GET_CALL_ACTIVITY = "RX.GET_CALL_ACTIVITY"

type IdParam struct {
	ID any `json:"_ID"`
}

type Message struct {
	Type   string `json:"type"`
	Value  string `json:"value"`
	Params any    `json:"params"`
	Raw    string
}

func (m *Message) UnmarshalJSON(data []byte) error {
	type RawMsg struct {
		Type   string          `json:"type"`
		Value  string          `json:"value"`
		Params json.RawMessage `json:"params"`
	}
	var v RawMsg
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	m.Type = v.Type
	m.Value = v.Value
	switch m.Type {
	case "RX.TEXT":
		var p IdParam
		if err := json.Unmarshal(v.Params, &p); err != nil {
			return err
		}
		var err error
		p.ID, err = getId(p.ID)
		if err != nil {
			return err
		}
		m.Params = p
	case "RX.ACTIVITY":
		var p RxActivityMsg
		if err := json.Unmarshal(v.Params, &p); err != nil {
			return err
		}
		var err error
		p.ID, err = getId(p.ID)
		if err != nil {
			return err
		}
		p.DialDec = float32(p.Dial) / 1e6
		m.Params = p
	case "RX.DIRECTED":
		var p DirectedMsg
		if err := json.Unmarshal(v.Params, &p); err != nil {
			return err
		}
		var err error
		p.ID, err = getId(p.ID)
		if err != nil {
			return err
		}
		p.LocalTime = time.UnixMilli(int64(p.UTC))
		p.DialDec = float32(p.Dial) / 1e6
		m.Params = p
	}
	return nil
}

type DirectedMsg struct {
	ID        any     `json:"_ID"`
	Cmd       string  `json:"CMD"`
	Extra     string  `json:"EXTRA"`
	From      string  `json:"FROM"`
	To        string  `json:"TO"`
	Grid      string  `json:"GRID"`
	Text      string  `json:"TEXT"`
	Dial      int     `json:"DIAL"`
	Freq      int     `json:"FREQ"`
	Offset    int     `json:"OFFSET"`
	SNR       int     `json:"SNR"`
	Speed     int     `json:"SPEED"`
	TDrift    float64 `json:"TDRIFT"`
	UTC       int64   `json:"UTC"`
	LocalTime time.Time
	DialDec   float32
}

type RxActivityMsg struct {
	ID      any     `json:"_ID"`
	Dial    int     `json:"DIAL"`
	Freq    int     `json:"FREQ"`
	Offset  int     `json:"OFFSET"`
	SNR     int     `json:"SNR"`
	Speed   int     `json:"SPEED"`
	TDrift  float64 `json:"TDRIFT"`
	UTC     int64   `json:"UTC"`
	DialDec float32
}

type FormMsg struct {
	Message string
}

type Station struct {
	Name     string
	Grid     string
	Messages []Message
}

func NewStation(name string) *Station {
	s := Station{Name: name}
	s.Messages = make([]Message, 0)
	return &s
}

func (s *Station) DirectedMessages() []DirectedMsg {
	d := make([]DirectedMsg, 0)
	for _, m := range s.Messages {
		if m.Type != "RX.DIRECTED" {
			continue
		}
		d = append(d, m.Params.(DirectedMsg))
	}
	return d
}

func (s *Station) NewHBMsg() Message {
	grid := s.Grid
	if len(grid) > 4 {
		grid = grid[0:4]
	}
	return Message{
		Type:  TYPE_TX_SEND,
		Value: fmt.Sprintf("%s: @HB HEARTBEAT %s", s.Name, grid),
	}
}

func (s *Station) NewCQMsg() Message {
	grid := s.Grid
	if len(grid) > 4 {
		grid = grid[0:4]
	}
	return Message{
		Type:  TYPE_TX_SEND,
		Value: fmt.Sprintf("%s: @ALLCALL CQ CQ CQ %s", s.Name, grid),
	}
}

func reverseArray(a []string) []string {
	for i, j := 0, len(a)-1; i < j; i, j = i+1, j-1 {
		a[i], a[j] = a[j], a[i]
	}
	return a
}

func SendMessage(msg Message) error {
	addr := net.UDPAddr{
		Port: 2242,
		IP:   net.ParseIP("127.0.0.1"),
	}
	buffer := make([]byte, 65500)
	conn, err := net.ListenUDP("udp", &addr)
	if err != nil {
		return err
	}
	defer conn.Close()
	_, remoteaddr, err := conn.ReadFromUDP(buffer)

	if err != nil {
		fmt.Printf("Some error %v", err)
		return err
	}

	jsonMsg, err := json.Marshal(msg)
	_, err = conn.WriteToUDP(jsonMsg, remoteaddr)
	if err != nil {
		fmt.Printf("Error sending %v", err)
	}

	fmt.Println("Message Sent!")
	return nil
}

func getMessage(msg Message) Message {
	addr := net.UDPAddr{
		Port: 2242,
		IP:   net.ParseIP("127.0.0.1"),
	}
	buffer := make([]byte, 65500)
	conn, err := net.ListenUDP("udp", &addr)
	if err != nil {
		fmt.Printf("Some error %v", err)
		return Message{}
	}
	defer conn.Close()
	_, remoteaddr, err := conn.ReadFromUDP(buffer)

	jsonMsg, err := json.Marshal(msg)
	_, err = conn.WriteToUDP(jsonMsg, remoteaddr)
	if err != nil {
		fmt.Printf("Error sending %v", err)
	}
	respLen, remoteaddr, err := conn.ReadFromUDP(buffer)
	resp := buffer[:respLen]
	respMsg, err := decodeMessage(resp)
	if err != nil {
		fmt.Printf("Error decoding %v\n%s\n", err, string(buffer))
	}
	go writeResponse(resp)
	return respMsg
}

func decodeMessage(resp []byte) (respMsg Message, err error) {
	err = json.Unmarshal(resp, &respMsg)
	respMsg.Raw = string(resp)
	if err != nil {
		return
	}
	return
}

func getId(v interface{}) (string, error) {
	switch v := v.(type) {
	case float64:
		if math.Abs(float64(v)+1.0) < 0.001 {
			return "-1", nil
		}
		return "", fmt.Errorf("unknown float id: %v", v)
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("conversion to string from %T not supported", v)
	}
}

func writeResponse(resp []byte) {
	f, err := os.OpenFile("msgs.log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.Write(resp); err != nil {
		log.Println(err)
	}
	if _, err := f.WriteString("\n"); err != nil {
		log.Println(err)
	}
}

func GetCallActivity() Message {
	msg := Message{
		Type:  "RX.GET_TEXT",
		Value: "",
	}

	return getMessage(msg)
}

func GetCallSign() string {
	msg := Message{
		Type:  "STATION.GET_CALLSIGN",
		Value: "",
	}
	return getMessage(msg).Value
}

func GetGrid() string {
	msg := Message{
		Type:  "STATION.GET_GRID",
		Value: "",
	}
	return getMessage(msg).Value
}
