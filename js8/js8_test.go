package js8

import (
	"testing"
	"time"
)

func TestDecodeEmptyRxText(t *testing.T) {
	data := `{"params":{"_ID":"176523134196"},"type":"RX.TEXT","value":""}`
	got, err := decodeMessage([]byte(data))
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	want := Message{
		Type:   "RX.TEXT",
		Value:  "",
		Params: IdParam{ID: "176523134196"},
		Raw:    data,
	}
	if got != want {
		t.Errorf("got %+v, want %+v", got, want)
	}
}

func TestDecodeRxActivity(t *testing.T) {
	data := `{"params":{"DIAL":7078000,"FREQ":7078563,"OFFSET":563,"SNR":-7,"SPEED":0,"TDRIFT":0.6000000238418579,"UTC":1675822211994,"_ID":-1},"type":"RX.ACTIVITY","value":"KA6ABC: KV4ABC HEARTBEAT SNR -08 "}`
	got, err := decodeMessage([]byte(data))
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	want := Message{
		Type:  "RX.ACTIVITY",
		Value: "KA6ABC: KV4ABC HEARTBEAT SNR -08 ",
		Params: RxActivityMsg{
			ID:      "-1",
			Dial:    7078000,
			DialDec: 7.078,
			Freq:    7078563,
			Offset:  563,
			SNR:     -7,
			Speed:   0,
			TDrift:  0.6000000238418579,
			UTC:     int64(1675822211994),
		},
		Raw: data,
	}

	if got != want {
		t.Errorf("error:\ngot  %+v\nwant %+v", got, want)
	}
}

func TestDecodeDirected(t *testing.T) {
	data := `{"params":{"CMD":" HEARTBEAT SNR","DIAL":7078000,"EXTRA":"-13","FREQ":7078564,"FROM":"W6ABC","GRID":" EM25","OFFSET":564,"SNR":-11,"SPEED":0,"TDRIFT":0.699999988079071,"TEXT":"W6ABC: N0ABC/0 HEARTBEAT SNR -13 ♢ ","TO":"N0ABC/0","UTC":1675822437051,"_ID":-1},"type":"RX.DIRECTED","value":"W6ABC: N0ABC/0 HEARTBEAT SNR -13 ♢ "}`
	got, err := decodeMessage([]byte(data))
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	want := Message{
		Type:  "RX.DIRECTED",
		Value: "W6ABC: N0ABC/0 HEARTBEAT SNR -13 ♢ ",
		Params: DirectedMsg{
			ID:        "-1",
			Cmd:       " HEARTBEAT SNR",
			Extra:     "-13",
			From:      "W6ABC",
			To:        "N0ABC/0",
			Grid:      " EM25",
			Text:      "W6ABC: N0ABC/0 HEARTBEAT SNR -13 ♢ ",
			Dial:      7078000,
			DialDec:   7.078,
			Freq:      7078564,
			Offset:    564,
			SNR:       -11,
			Speed:     0,
			TDrift:    0.699999988079071,
			UTC:       int64(1675822437051),
			LocalTime: time.UnixMilli(int64(1675822437051)),
		},
		Raw: data,
	}

	if got != want {
		t.Errorf("error:\ngot  %+v\nwant %+v", got, want)
	}
}
