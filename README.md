# kk7agr
This repository contains software created for amateur radio, initially focused on the JS8 protocol: http://js8call.com/

## Getting started
Requirements:
Go 1.18 or newer

If you do not have go installed, download it here: https://go.dev/dl/
Then, follow the installation instructions: https://go.dev/doc/install

Next, clone the git repository:

`git clone https://gitlab.com/gcolburn/kk7agr.git`

## Name
TBD

## Description
Currently the focus is on building a self-contained go web application that interacts with the
JS8Call JSON API to display received messages, as well as send messages. Long term, the goal is
to build a distributed system for storing JS8 messages from stations for use in emergency and other
ham radio communications. Other use cases include sending/receiving messages from locations without
cellular or local repeaters.

As a web application, it can be accessed from a mobile device, desktop, etc. without having to deploy native
mobile apps.

## Run
`go run .`

Open a browser to http://localhost:8080, or alternately the ip address of the computer
it is running from with port 80 for remote access from another computer on the network.

## Test and Deploy
To run tests, run: `go test ./...`

## Installation
TODO: Describe how to build binaries and deploy on varios OS's.

## Usage
TODO

## Support
Please submit an issue for support.

## Roadmap
TODO

## Contributing
Please submit an issue

## License
TBD

## Project status
This project is just getting started as an experiment.